using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageTrigger : MonoBehaviour
    {
    public GameObject prefab;
    Image image;
    public ArTapToPlace artap;
    private void Awake ()
        {
        artap = FindObjectOfType<ArTapToPlace> ();
        }
    public void Click ()
        {
        artap.objectToPlace = prefab;
        artap.text.gameObject.SetActive (false);
        if (artap.objectPlaced != null)
            {
            artap.PlaceObject ();
            }

        }
    }