using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class ButtonBorrar : MonoBehaviour
{
    public ArTapToPlace artap;
    public void Borrar ()
        {
        if (artap.lista.Count > 0)
            {
            foreach (var item in artap.lista)
                {
                LeanSelectableByFinger lsbf = item.GetComponentInChildren<LeanSelectableByFinger>();
                if (lsbf.IsSelected == true)
                    {
                    artap.lista.Remove (item);
                    Destroy (lsbf.gameObject);
                    return;
                    }

                }
            }
        if (artap.objectPlaced != null)//Busco el objeto seleccionado en la lista de objetos
            {
            artap.objectPlaced.GetComponent<Effect> ().destruir = true;
            artap.objectPlaced = null;
            }
        }
    }
