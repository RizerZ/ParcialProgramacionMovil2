using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ArTapToPlace : MonoBehaviour
{
    public GameObject placementIndicator;
    private Pose PlacementPose;
    private ARRaycastManager aRRaycastManager;
    private bool placementPoseIsValid = false;
    private bool floor;

    public bool Floor
        {
        get
            {
            if (placementIndicator.activeSelf)
                {
                floor = true;
                return floor;
                }
            else
                {
                floor = false;
                return floor;
                }
            }
        }

    private bool objectInScene;

    public bool ObjectInScene
        {
        get
            {
            if (objectPlaced != null)
                {
                objectInScene = true;
                return objectInScene;
                }
            else
                {
                objectInScene = false;
                return objectInScene;
                }
            }
        }



    public Text text;
    //public List<GameObject> lista;

    public GameObject objectToPlace;
    public GameObject objectPlaced;

    public List<GameObject> lista;

    [SerializeField] private Camera arCamera;
    public bool selected;
    // Start is called before the first frame update
    void Start()
    {
        aRRaycastManager = FindObjectOfType<ARRaycastManager> ();
        lista = new List<GameObject> ();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Floor && !ObjectInScene)
            {
            text.text = "Detectando el piso";
            }
        if (Floor && ObjectInScene || Floor && objectToPlace != null)
            {
            text.text = "";
            }

        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began)
            {
            PlaceObject ();
            }
        if (objectPlaced == null)
            {
            UpdatePlacementPose ();
            UpdatePlacementIndicator ();//muestra y oculta el indicador

            if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began)
                {
                PlaceObject ();//instancia un objeto
                }
            }
        else
            {
            placementIndicator.SetActive (false);
            }

        if (Input.GetKeyDown (KeyCode.W))
            {
            Debug.Log ("ASDFASDF");
            if (objectToPlace == null)
                {
                text.gameObject.SetActive (true);
                }
            else
                {
                if (objectPlaced != null)
                    {
                    Destroy (objectPlaced);
                    objectPlaced = Instantiate (objectToPlace);
                    }
                else
                    {
                    objectPlaced = Instantiate (objectToPlace);
                    //lista.Add(Instantiate(objectToPlace));
                    }
                objectToPlace = null;
                }
            }
        }
    public void PlaceObject ()
        {
        if (objectToPlace == null)
            {
            text.gameObject.SetActive (true);
            }
        else
            {
            if (objectPlaced != null)
                {
                Vector3 pos = new Vector3(objectPlaced.transform.position.x, objectPlaced.transform.position.y, objectPlaced.transform.position.z);
                Quaternion rot = new Quaternion(objectPlaced.transform.rotation.x,
                    objectPlaced.transform.rotation.y,
                    objectPlaced.transform.rotation.z,
                    objectPlaced.transform.rotation.w);
                Destroy (objectPlaced);
                objectPlaced = Instantiate (objectToPlace, pos, rot);
                }
            else
                {
                //objectPlaced = Instantiate (objectToPlace, PlacementPose.position, PlacementPose.rotation);
                objectPlaced = Instantiate (objectToPlace, placementIndicator.transform.position, placementIndicator.transform.rotation);
                //objectPlaced = Instantiate (objectToPlace, new Vector3(placementIndicator.transform.position.x, transform.position.y, placementIndicator.transform.position.z + 0.3f),placementIndicator.transform.rotation);
                }
            objectToPlace = null;
            }

        }


    private void UpdatePlacementIndicator ()
        {
        if (placementPoseIsValid)
            {
            placementIndicator.SetActive (true);
            placementIndicator.transform.SetPositionAndRotation (PlacementPose.position, PlacementPose.rotation);
            }
        else
            {
            placementIndicator.SetActive (false);
            //text.text = "DETECTANDO EL PISO";
            }
        }
    private void UpdatePlacementPose ()
        {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        aRRaycastManager.Raycast (screenCenter, hits, TrackableType.Planes);
        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
            {
            //text.gameObject.SetActive (false);
            PlacementPose = hits[0].pose;
            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            PlacementPose.rotation = Quaternion.LookRotation (cameraBearing);
            }
        }
    }