using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Luz : MonoBehaviour
{
    public Color color;
    public GameObject luz;
    private Image image;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image> ();
        image.color = color;
    }


    public void Clic ()
        {
        luz.GetComponent<Light>().color = image.color;
        }
}
