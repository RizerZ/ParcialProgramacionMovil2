using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Acelerometro : MonoBehaviour
    {
    public ArTapToPlace art;
    public Light light;
    public bool on=false;
    public Text acelerometro;
    public Text rgb;
    // Start is called before the first frame update
    void Start ()
        {

        }

    // Update is called once per frame
    void Update ()
        {
        if (on)
            {
            accelerometer ();
            }

        }

    void accelerometer ()
        {
        Vector3 dir = Vector3.zero;
        dir.y = Math.Abs(Input.acceleration.y);
        dir.x = Math.Abs(Input.acceleration.x);
        dir.z = Math.Abs (Input.acceleration.z);
        acelerometro.text = "Acelerómetro: x=" + dir.x + " y=" + dir.y + " z=" + dir.z;
        if (dir.sqrMagnitude > 1)
            {
            
            Color color= new Color(dir.normalized.x*255,(dir.normalized.y*255)/100,dir.normalized.z*255);
            rgb.text = "R=" + (int)color.r + " G=" + (int)color.g + " B=" + (int)color.b;

            light.color = color;

            }
        }


    public void Clic ()
        {
        on = !on;
        }
    }


