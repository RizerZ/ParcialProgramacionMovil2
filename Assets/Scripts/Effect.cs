using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    public float escala = 0.1f;
    public float velocidad = 1.0f;
    public bool destruir = false;
    // Start is called before the first frame update
    void Start ()
        {
        this.transform.localScale = new Vector3 (escala, escala, escala);
        }

    // Update is called once per frame
    void Update ()
        {
        //Agrandar();


        Destruir ();

        }

    public void Agrandar ()
        {
        if (escala != 1 && !destruir)
            {
            escala = escala + (velocidad * Time.deltaTime);

            transform.localScale = new Vector3 (escala, escala, escala);

            if (escala > 1)
                {
                escala = 1;
                }
            }
        }

    public void Destruir ()
        {
        if (destruir)
            {
            /*
            escala = escala - (velocidad * Time.deltaTime);
            transform.localScale = new Vector3(escala, escala, escala);
            if (escala < 0)
            {
                Destroy(this.gameObject);
            }*/

            Destroy (this.gameObject);
            }
        }
    }
